<?php

class Caroufredsel extends ImageGalleryUI {
	public $item_template = "Caroufredsel_item";
	public $layout_template = "Caroufredsel_layout";
	public function initialize() {
		Requirements::javascript(THIRDPARTY_DIR.'/jquery/jquery.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/caroufredsel/javascript/jquery.caroufredsel.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/caroufredsel/javascript/caroufredsel_init.js');
		Requirements::css(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/caroufredsel/css/caroufredsel.css');
	}

}