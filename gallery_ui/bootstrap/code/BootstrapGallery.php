<?php
/**
 * Created by PhpStorm.
 * User: ed
 * Date: 29/10/14
 * Time: 1:29 PM
 */

class BootstrapGallery extends ImageGalleryUI {
	public $item_template = "BootstrapGallery_item";
	public $layout_template = "BootstrapGallery_layout";
	public function initialize() {
		Requirements::javascript(THIRDPARTY_DIR.'/jquery/jquery.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/bootstrap/javascript/jquery.mobile.custom.min.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/bootstrap/javascript/bootstrap-image-gallery.min.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/bootstrap/javascript/custom.js');
	}
}