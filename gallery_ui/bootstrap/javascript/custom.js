/**
 * Created by ed on 29/10/14.
 */
    // carousel
$("#image-carousel").on('swiperight', function () {
    $(this).carousel('prev');
});
$("#image-carousel").on('swipeleft', function () {
    $(this).carousel('next');
});
$('#image-carousel').on('slid.bs.carousel', function () {
    var index = $(this).find('.item.active').index();
    $('[id^=carousel-selector-]')
        .removeClass('selected').addClass('not-selected')
        .eq(index)
        .addClass('selected').removeClass('not-selected')
});
$('[id^=carousel-selector-]').click( function(){
    $(this).addClass('selected').removeClass('not-selected').siblings().not(this).removeClass('selected').addClass('not-selected');
    var id_selector = $(this).attr("id").split('-');
    var id = id_selector[id_selector.length - 1];
    $('#image-carousel').carousel(parseInt(id) - 1);
});
$('.gallery-images').hover(function() {
    $(this).toggleClass('active').parent().siblings().find('.gallery-images').not(this).toggleClass('inactive');
});