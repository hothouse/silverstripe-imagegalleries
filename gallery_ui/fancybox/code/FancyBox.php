<?php

class FancyBox extends ImageGalleryUI {
	public $item_template = "FancyBox_item";
	public function initialize() {
		Requirements::javascript(THIRDPARTY_DIR.'/jquery/jquery.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/fancybox/javascript/jquery.fancybox.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/fancybox/javascript/jquery.pngFix.pack.js');
		Requirements::javascript(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/fancybox/javascript/fancybox_init.js');
		Requirements::css(MODULE_IMAGEGALLERY_PATH.'/gallery_ui/fancybox/css/fancy.css');
	}

}