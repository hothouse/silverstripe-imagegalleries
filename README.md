#Silverstripe image gallery

##Setup
The newest iteration of this module uses the blueimp js gallery and is called "BootstrapGallery". It's not enabled by default for legacy reasons. 

How to enable:
 
 - add entry in _config.yml
 
~~~~
ImageGalleryPage:
   GalleryUI: 'BootstrapGallery'
~~~~
 
 - import the sass file in the bootstrap.scss
 
~~~~
@import "../../../silverstripe-imagegalleries/gallery_ui/bootstrap/sass/gallery/imagegallery";
~~~~
 
 - For best use please add these submodules:
 
~~~~
git://github.com/ajshort/silverstripe-gridfieldextensions.git
git://github.com/colymba/GridFieldBulkEditingTools.git
~~~~ 
 
That should be it!