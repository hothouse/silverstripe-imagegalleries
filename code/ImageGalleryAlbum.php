<?php

class ImageGalleryAlbum extends DataObject {
	private static $db = array (
		'AlbumName' => 'Varchar(255)',
		'URLSegment' => 'Varchar(255)',
		'Description' => 'Text',
		'SortID' => 'Int'
	);

	private static $has_one = array (
		'CoverImage' => 'Image',
		'ImageGalleryPage' => 'ImageGalleryPage'
	);

	private static $has_many = array (
		'GalleryItems' => 'ImageGalleryItem'
	);

	private static $summary_fields = array(
		'AlbumName' => 'Album',
		'Description' => 'Description',
		'CoverImage.CMSThumbnail' => 'CoverImage'
	);

	private static $default_sort = 'SortID';

	public function getTitle() {
		return $this->AlbumName;
	}

	public function getCMSFields() {
		// get fields
		$fields = parent::getCMSFields();

		if($this->ID) {
			// foldername
			$FolderName = $this->ImageGalleryPage()->RelativeLink(Convert::raw2url($this->AlbumName));

			// photos
			$gridFieldConfig = GridFieldConfig::create()->addComponents(
				new GridFieldToolbarHeader(),
				new GridFieldAddNewButton('toolbar-header-right'),
				new GridFieldSortableHeader(),
				new GridFieldDataColumns(),
				new GridFieldPaginator(50),
				new GridFieldEditButton(),
				new GridFieldDeleteAction(),
				new GridFieldDetailForm(),
				class_exists('GridFieldOrderableRows') ?
				  new GridFieldOrderableRows('SortID') :
				  (class_exists('GridFieldSortableRows') ? new GridFieldSortableRows('SortID') : null)
			);
			if(class_exists('GridFieldBulkManager')) {
				$gridFieldConfig
					->addComponent(new GridFieldBulkUpload());

				// set upload folder
				$gridFieldConfig->getComponentByType('GridFieldBulkUpload')->setUfSetup('setFolderName', $FolderName);
			}
			$gridField = new GridField("GalleryItems", "GalleryItems", $this->GalleryItems(), $gridFieldConfig);
			$fields->addFieldToTab("Root.Photos", $gridField);

			// image
			$fields->addFieldToTab("Root.Main", Uploadfield::create('CoverImage')->setFolderName($FolderName));
		} else {
			$fields->removeByName('CoverImage');
		}

		// remove fields
		$fields->removeByName('GalleryItems');
		$fields->removeByName('ImageGalleryPageID');
		$fields->removeByName('URLSegment');
		$fields->removeByName('SortID');
		return $fields;
	}

	public function Link() {
		return $this->ImageGalleryPage()->Link('album/'.$this->URLSegment);
	}

	public function LinkingMode() {
		return Controller::curr()->urlParams['ID'] == $this->URLSegment ? "current" : "link";
	}

	public function FormattedCoverImage() {
		return $this->CoverImage()->CroppedImage($this->ImageGalleryPage()->CoverImageWidth,$this->ImageGalleryPage()->CoverImageHeight);
	}

	public function onBeforeWrite() {
		$this->URLSegment = Convert::raw2url($this->AlbumName);
		parent::onBeforeWrite();
	}

	public function onBeforeDelete() {
		parent::onBeforeDelete();
		$this->GalleryItems()->removeAll();
	}

}
