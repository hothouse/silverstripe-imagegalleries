<?php

class ImageGalleryItem extends DataObject
{
	protected $UI;

	private static $delete_permission = "CMS_ACCESS_CMSMain";

	private static $db = array (
		'Caption' => 'Text',
		'SortID' => 'Int'
	);

	private static $has_one = array (
		'Album' => 'ImageGalleryAlbum',
		'Image' => 'Image'
	);

	private static $summary_fields = array(
		'Caption' => 'Caption',
		'Image.CMSThumbnail' => 'Image'
	);

	private static $default_sort = 'SortID';

	public function getCMSFields() {
		// get fields
		$fields = parent::getCMSFields();

		if($this->ID) {
			$FolderName = $this->Album()->ImageGalleryPage()->RelativeLink(Convert::raw2url($this->Album()->AlbumName));
			$fields->addFieldToTab("Root.Main", Uploadfield::create('Image')->setFolderName($FolderName));
		} else {
			$fields->removeByName('Image');
		}

		// remove fields
		$fields->removeByName('AlbumID');
		$fields->removeByName('SortID');
		return $fields;
	}

	public function onBeforeDelete() {
		if($this->Image()->exists()) {
			$this->Image()->delete();
		}
		parent::onBeforeDelete();
	}

	public function setUI(ImageGalleryUI $ui) {
		$this->UI = $ui;
	}

	public function GalleryItem() {
		if($this->UI)
			return $this->renderWith(array($this->UI->item_template));
		return false;
	}

	public function canDelete($member = NULL) {
		return Permission::check(self::$delete_permission);
	}
}
